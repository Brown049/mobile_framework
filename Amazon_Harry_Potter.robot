*** Settings ***

Library           AppiumLibrary
Library           String

Suite Teardown    Stop Screen Recording    

*** Test Cases ***

Launch Application - Amazon Shopping Select “Kindle Store” option as a sub category.
    

    Open Application                    http://localhost:4723/wd/hub  platformName=Android   deviceName=R58N3057XWW  noReset=true  newCommandTimeout=300  appPackage=com.amazon.mShop.android.shopping  appActivity=com.amazon.mShop.splashscreen.StartupActivity  automationName=Uiautomator2
    Start Screen Recording    
    Sleep                               4s
    
    Capture Page Screenshot
    
    #Occasionally asking for sign in.  The below handles that.
    #Run Keyword And Continue On Failure  Click Element  id=com.amazon.mShop.android.shopping:id/skip_sign_in_button

Select the “Books and Reading” option from the category tab available at the bottom of screen and select “Kindle Store” option as a sub category

    Click Element                       xpath=/hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[3]/android.view.ViewGroup/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[4]

    Wait Until Page Contains Element    xpath=//android.widget.ViewGroup[@content-desc="Fashion & Beauty"]  timeout=30s
    
    Swipe Up                            com.amazon.mShop.android.shopping:id/fragment_switch_view_container
    Capture Page Screenshot

    Tap                                 xpath=//android.widget.ViewGroup[@content-desc="Books and Reading"]

    Sleep                               1s
    
    Tap                                 xpath=//android.widget.ViewGroup[@content-desc="Kindle Store"]
    Sleep                               1s

Search for the book named - Harry Potter and the Philosopher's Stone

    Tap                                 com.amazon.mShop.android.shopping:id/chrome_search_hint_view    
    Input Text                          id=com.amazon.mShop.android.shopping:id/rs_search_src_text    Harry Potter and the Philosopher's Stone
    Tap                                 com.amazon.mShop.android.shopping:id/chrome_action_bar_search_icon
    #Hit 'Enter'
    Press Keycode                       66
    Sleep                               2s
    Capture Page Screenshot
    
Check 'Harry Potter and the Philosopher's Stone' is in search results

    Element Should Contain Text    	    //hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView    Harry Potter and the Philosopher's Stone

Check if 'Harry Potter and the Philosopher's Stone' is a best seller?
 
    Element Should Contain Text         //android.view.View[@content-desc="Best Seller Harry Potter and the Philosopher's Stone"]/android.view.View[1]/android.view.View/android.view.View  Best Seller  msg=Not Best Seller

Check if 'Harry Potter and the Philosopher's Stone' has reviews and how many?

  
    ${reviews}=                         Get Text    //android.view.View[@content-desc="Harry Potter and the Philosopher's Stone by J.K. Rowling 4.8 out of 5 stars 36,096"]/android.view.View[3]
    Log To Console                      ${reviews}
    Log                                 ${reviews}
    ${Num_of_reviews}=                  Remove String         ${reviews}  ,
    Log                                 ${Num_of_reviews}
    ${Num_of_reviews}=                  Convert To Integer    ${Num_of_reviews}
    
    Run Keyword If    ${Num_of_reviews} > 1  Log to Console  \n${Num_of_reviews} reviews are available\n
    ...    ELSE                              Log To Console    \nNo Reviews Available\n                  
    
Enable the prime option ON and check if the book is still available?
    
    Tap                                 accessibility_id=Kindle Store
    Sleep                               2
    Capture Page Screenshot    
    Tap                                 xpath=//android.view.View[@content-desc="Prime Eligible"]/android.widget.Image[2]
    Capture Page Screenshot    
    Sleep                               1
    Element Should Contain Text    	    //android.view.View[@content-desc="Harry Potter and the Philosopher's Stone, Book 1 by J.K. Rowling 4.8 out of 5 stars 36,096"]/android.view.View[1]    Harry Potter and the Philosopher's Stone

Select the book and add it into the basket. Go to the basket and verify that the selected book is listed there
    
    Tap       	                       	//android.view.View[@content-desc="Best Seller Harry Potter and the Philosopher's Stone, Book 1"]/android.view.View[2]/android.view.View/android.widget.Image

    Sleep                               1
    
    Swipe Up                            com.amazon.mShop.android.shopping:id/mash_web_fragment
    #Tap 'Paper Back'
    Click Element       	           	//hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[2]/android.view.View/android.view.View/android.view.View[4]/android.widget.Button
 
    Swipe Up                            com.amazon.mShop.android.shopping:id/fragment_switch_view_container
    Swipe Up                            com.amazon.mShop.android.shopping:id/fragment_switch_view_container
    Swipe Up                            com.amazon.mShop.android.shopping:id/fragment_switch_view_container
    Capture Page Screenshot    
    #Click 'Add to Basket'
    Click Element                       //hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.FrameLayout[2]/android.widget.FrameLayout/android.widget.RelativeLayout/android.widget.RelativeLayout/android.webkit.WebView/android.webkit.WebView/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View/android.view.View[2]/android.view.View[1]/android.view.View/android.view.View/android.view.View[1]/android.view.View/android.view.View[1]/android.view.View/android.view.View/android.view.View/android.view.View[3]/android.view.View/android.view.View/android.view.View/android.widget.Button
    #Tap Basket from bottom menu
    Tap  	                            //hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.view.ViewGroup/android.widget.LinearLayout[3]/android.view.ViewGroup/android.widget.HorizontalScrollView/android.widget.LinearLayout/android.support.v7.app.ActionBar.Tab[3]
    #Verify book present in basket
    Element Should Contain Text    	    //android.view.View[@content-desc="Harry Potter and the Philosopher's Stone: 1/7 (Harry Potter, 1) "]/android.widget.TextView    Harry Potter and the Philosopher's Stone: 1/7 (Harry Potter, 1)

*** Keywords ***

Swipe Up
    [Arguments]    ${Sample_content_fragment}
    ${element_size}=    Get Element Size    id=${Sample_content_fragment}
    ${element_location}=    Get Element Location    id=${Sample_content_fragment}
    ${start_x}=         Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
    ${start_y}=         Evaluate      ${element_location['y']} + (${element_size['height']} * 0.7)
    ${end_x}=           Evaluate      ${element_location['x']} + (${element_size['width']} * 0.5)
    ${end_y}=           Evaluate      ${element_location['y']} + (${element_size['height']} * 0.3)
    Swipe               ${start_x}    ${start_y}  ${end_x}  ${end_y}  500
    Sleep  1